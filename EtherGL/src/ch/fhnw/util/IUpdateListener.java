package ch.fhnw.util;


public interface IUpdateListener {
	void requestUpdate(Object source);
}
