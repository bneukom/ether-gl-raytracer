package ch.fhnw.ether.render.raytracer.materials.textures.procedural;

import ch.fhnw.ether.render.raytracer.materials.textures.IRayTracingTexure;

public abstract class ProceduralTexture implements IRayTracingTexure {
	protected final float scale;

	public ProceduralTexture(float scale) {
		this.scale = scale;
	}

}
