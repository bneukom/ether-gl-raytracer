package ch.fhnw.ether.render.raytracer.materials.textures;

import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.Vec2;
import ch.fhnw.util.math.Vec3;

public interface IRayTracingTexure {

	/**
	 * Returns the {@link RGB} of the pixel at the given position.
	 * 
	 * @param origin
	 * @param mapped
	 * @param w
	 * @param h
	 * 
	 * @return
	 */
	public RGB getPixel(Vec3 origin, Vec2 mapped, int w, int h);
}
