package ch.fhnw.ether.render.raytracer.materials.builtin;

import java.net.URL;

import ch.fhnw.ether.render.raytracer.materials.IRayTracingMaterial;
import ch.fhnw.ether.render.raytracer.materials.textures.IRayTracingTexure;
import ch.fhnw.ether.render.raytracer.materials.textures.NormalMap;
import ch.fhnw.ether.render.raytracer.materials.textures.RayTracingTexture;
import ch.fhnw.util.color.RGB;

public class WoodMaterial implements IRayTracingMaterial {
	private final RayTracingTexture texture;

	public WoodMaterial() {
		URL resource = getClass().getClassLoader().getResource("ch/fhnw/ether/render/raytracer/materials/builtin/assets/wood.jpg");
		texture = new RayTracingTexture(resource, false);
	}

	@Override
	public float getRefractiveIndex() {
		return 1;
	}

	@Override
	public float getReflection() {
		return 0;
	}

	@Override
	public boolean isTransparent() {
		return false;
	}

	@Override
	public float getShininess() {
		return 1f;
	}

	@Override
	public RGB getSpecular() {
		return new RGB(0, 0, 0f);
	}

	@Override
	public IRayTracingTexure getTexture() {
		return texture;
	}

	@Override
	public NormalMap getNormalMap() {
		return null;
	}

}