package ch.fhnw.ether.render.raytracer.materials.builtin;

import ch.fhnw.ether.render.raytracer.materials.IRayTracingMaterial;
import ch.fhnw.ether.render.raytracer.materials.textures.IRayTracingTexure;
import ch.fhnw.ether.render.raytracer.materials.textures.NormalMap;
import ch.fhnw.ether.render.raytracer.materials.textures.SimpleColorTexture;
import ch.fhnw.util.color.RGB;

public class GlassMaterial implements IRayTracingMaterial {

	final SimpleColorTexture simpleColorTexture = new SimpleColorTexture(new RGB(0, 0, 0));

	@Override
	public float getRefractiveIndex() {
		return 1.5f;
	}

	@Override
	public float getReflection() {
		return 0f;
	}

	@Override
	public boolean isTransparent() {
		return true;
	}

	@Override
	public float getShininess() {
		return 1f;
	}

	@Override
	public RGB getSpecular() {
		return new RGB(0, 0, 0);
	}

	@Override
	public IRayTracingTexure getTexture() {
		return simpleColorTexture;
	}

	@Override
	public NormalMap getNormalMap() {
		// TODO Auto-generated method stub
		return null;
	}

}
