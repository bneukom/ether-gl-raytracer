package ch.fhnw.ether.render.raytracer.materials.builtin;

import ch.fhnw.ether.render.raytracer.materials.IRayTracingMaterial;
import ch.fhnw.ether.render.raytracer.materials.textures.IRayTracingTexure;
import ch.fhnw.ether.render.raytracer.materials.textures.NormalMap;
import ch.fhnw.util.color.RGB;

public class RayTracingMaterialAdapter implements IRayTracingMaterial {

	@Override
	public NormalMap getNormalMap() {
		return null;
	}

	@Override
	public float getRefractiveIndex() {
		return 0;
	}

	@Override
	public boolean isTransparent() {
		return false;
	}

	@Override
	public float getReflection() {
		return 0;
	}

	@Override
	public RGB getSpecular() {
		return new RGB(0, 0, 0);
	}

	@Override
	public float getShininess() {
		return 0;
	}

	@Override
	public IRayTracingTexure getTexture() {
		return null;
	}

}
