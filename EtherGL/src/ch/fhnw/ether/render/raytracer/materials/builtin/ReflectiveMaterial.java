package ch.fhnw.ether.render.raytracer.materials.builtin;

import ch.fhnw.ether.render.raytracer.materials.IRayTracingMaterial;
import ch.fhnw.ether.render.raytracer.materials.textures.IRayTracingTexure;
import ch.fhnw.ether.render.raytracer.materials.textures.NormalMap;
import ch.fhnw.ether.render.raytracer.materials.textures.SimpleColorTexture;
import ch.fhnw.util.color.RGB;

public class ReflectiveMaterial implements IRayTracingMaterial {
	final SimpleColorTexture colorTexture = new SimpleColorTexture(new RGB(0f, 0f, 0f));
	final float reflection;

	public ReflectiveMaterial(final float reflection) {
		this.reflection = reflection;
	}

	public ReflectiveMaterial() {
		this(0.4f);
	}

	@Override
	public float getRefractiveIndex() {
		return 1f;
	}

	@Override
	public float getReflection() {
		return reflection;
	}

	@Override
	public boolean isTransparent() {
		return false;
	}

	@Override
	public float getShininess() {
		return 1f;
	}

	@Override
	public RGB getSpecular() {
		return new RGB(1, 1f, 1f);
	}

	@Override
	public IRayTracingTexure getTexture() {
		return colorTexture;
	}

	@Override
	public NormalMap getNormalMap() {
		return null;
	}

}