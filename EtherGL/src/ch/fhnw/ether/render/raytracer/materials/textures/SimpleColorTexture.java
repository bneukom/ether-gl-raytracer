package ch.fhnw.ether.render.raytracer.materials.textures;

import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.Vec2;
import ch.fhnw.util.math.Vec3;

public class SimpleColorTexture implements IRayTracingTexure {

	private RGB color;

	public SimpleColorTexture(RGB color) {
		super();
		this.color = color;
	}

	@Override
	public RGB getPixel(Vec3 origin, Vec2 p, int w, int h) {
		return color;
	}

	public RGB getColor() {
		return color;
	}

}
