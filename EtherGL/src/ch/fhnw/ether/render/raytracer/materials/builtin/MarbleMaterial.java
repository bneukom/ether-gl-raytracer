package ch.fhnw.ether.render.raytracer.materials.builtin;

import ch.fhnw.ether.render.raytracer.materials.IRayTracingMaterial;
import ch.fhnw.ether.render.raytracer.materials.textures.IRayTracingTexure;
import ch.fhnw.ether.render.raytracer.materials.textures.NormalMap;
import ch.fhnw.ether.render.raytracer.materials.textures.procedural.MarbleTexture;
import ch.fhnw.util.color.RGB;

public class MarbleMaterial implements IRayTracingMaterial {

	private final MarbleTexture texture;
	private NormalMap normalMap;

	public MarbleMaterial() {
		texture = new MarbleTexture(0.5f);
		normalMap = new NormalMap(0.5f, 0.25f);
	}

	@Override
	public float getRefractiveIndex() {
		return 1;
	}

	@Override
	public float getReflection() {
		return 0.2f;
	}

	@Override
	public boolean isTransparent() {
		return false;
	}

	@Override
	public float getShininess() {
		return 1f;
	}

	@Override
	public RGB getSpecular() {
		return new RGB(1, 1, 1f);
	}

	@Override
	public IRayTracingTexure getTexture() {
		return texture;
	}

	@Override
	public NormalMap getNormalMap() {
		return normalMap;
	}

}
