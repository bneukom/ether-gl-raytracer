package ch.fhnw.ether.render.raytracer.objects.parametric;

import java.util.EnumSet;
import java.util.Optional;

import ch.fhnw.ether.render.raytracer.IntersectionResult;
import ch.fhnw.ether.render.raytracer.materials.IRayTracingMaterial;
import ch.fhnw.ether.render.raytracer.objects.RayTracingObject;
import ch.fhnw.ether.render.raytracer.objects.parametric.surface.IParametricSurface;
import ch.fhnw.ether.render.raytracer.shaders.IRayTracingShader;
import ch.fhnw.ether.scene.mesh.geometry.IGeometry;
import ch.fhnw.util.math.Vec2;
import ch.fhnw.util.math.Vec3;
import ch.fhnw.util.math.geometry.BoundingBox;
import ch.fhnw.util.math.geometry.Line;

public class ParametricRayTracingObject extends RayTracingObject {

	private final IParametricSurface surface;

	private String name = "ray_trace_object";

	public ParametricRayTracingObject(final Vec3 position, final IParametricSurface surface, final IRayTracingShader shader, final IRayTracingMaterial material) {
		super(position, material, shader);
		this.surface = surface;
	}

	@Override
	public BoundingBox getBounds() {
		return new BoundingBox();
	}

	@Override
	public Pass getPass() {
		return Pass.DEPTH;
	}

	@Override
	public EnumSet<Flags> getFlags() {
		return NO_FLAGS;
	}

	@Override
	public IGeometry getGeometry() {
		return null;
	}

	@Override
	public boolean needsUpdate() {
		return false;
	}

	@Override
	public void requestUpdate(final Object source) {
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(final String name) {
		this.name = name;
	}


	public IParametricSurface getSurface() {
		return surface;
	}

	@Override
	public Optional<IntersectionResult> intersect(final Line ray) {
		final Vec3 point = surface.intersect(position, ray);
		
		// no intersection
		if (point == null) {
			return Optional.empty();
		}
		
		// find normal
		Vec3 normal = surface.getNormalAt(getPosition(), point);
		
		if (material.getNormalMap() != null) {
			Vec3 newNormal = material.getNormalMap().modifyNormal(normal, point);
			normal = newNormal;
		}
		
		return Optional.of(new IntersectionResult(this, point, normal, ray.getOrigin().subtract(point).length()));
	}

	@Override
	public Vec2 mapTexture(Vec3 v) {
		return surface.mapTexture(position, v);
	}

}
