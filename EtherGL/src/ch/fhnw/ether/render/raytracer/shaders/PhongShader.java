package ch.fhnw.ether.render.raytracer.shaders;

import ch.fhnw.ether.scene.light.ILight;
import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.Vec3;

public class PhongShader implements IRayTracingShader {

	@Override
	public RGB apply(RGB diffuseRgb, RGB specularRgb, Vec3 origin, Vec3 position, Vec3 normal, ILight light) {
		final Vec3 n = normal;
		final Vec3 p = position;
		final Vec3 l = light.getPosition().subtract(p).normalize();
		final Vec3 v = origin.subtract(p).normalize();
		final Vec3 h = l.add(v).normalize();

		float NdotL = IRayTracingShader.saturate(n.dot(l));
		float NdotH = IRayTracingShader.saturate(n.dot(h));

		final Vec3 diffuse = diffuseRgb.scale(NdotL);
		final Vec3 spec = specularRgb.scale((float) Math.pow(NdotH, 50));
		
		final Vec3 color = light.getColor().mul(diffuse.add(spec));

		return new RGB(color);
	}

}
