package ch.fhnw.ether.scene.light;

import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.Vec3;

public class PointLight extends GenericLight {

	public final int radius;

	public PointLight(Vec3 position, RGB ambient, RGB color) {
		this(position, ambient, color, Float.MAX_VALUE, 1);
	}

	public PointLight(Vec3 position, RGB ambient, RGB color, int radius) {
		this(position, ambient, color, Float.MAX_VALUE, radius);
	}

	public PointLight(Vec3 position, RGB ambient, RGB color, float range, int radius) {
		super(LightSource.pointSource(position, ambient, color, range));
		this.radius = radius;
	}
}
