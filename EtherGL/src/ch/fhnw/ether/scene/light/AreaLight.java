package ch.fhnw.ether.scene.light;

import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.Vec3;

public class AreaLight extends GenericLight {

	public final int width;
	public final int height;

	public AreaLight(Vec3 position, RGB ambient, RGB color, int width, int height) {
		super(LightSource.areaSource(position, ambient, color));
		this.width = width;
		this.height = height;
	}
}
