/*
 * Copyright (c) 2013 - 2014 Stefan Muller Arisona, Simon Schubiger, Samuel von Stachelski
 * Copyright (c) 2013 - 2014 FHNW & ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *  Neither the name of FHNW / ETH Zurich nor the names of its contributors may
 *   be used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */package ch.fhnw.ether.examples.raytracing;

import java.util.ArrayList;
import java.util.List;

import ch.fhnw.ether.controller.DefaultController;
import ch.fhnw.ether.controller.IController;
import ch.fhnw.ether.controller.event.EventDrivenScheduler;
import ch.fhnw.ether.render.raytracer.materials.builtin.MattMaterial;
import ch.fhnw.ether.render.raytracer.materials.builtin.ReflectiveMaterial;
import ch.fhnw.ether.render.raytracer.materials.textures.SimpleColorTexture;
import ch.fhnw.ether.render.raytracer.objects.parametric.ParametricRayTracingObject;
import ch.fhnw.ether.render.raytracer.objects.parametric.surface.PlaneSurface;
import ch.fhnw.ether.render.raytracer.objects.parametric.surface.SphereSurface;
import ch.fhnw.ether.render.raytracer.renderers.AbstractRayTracingRenderer;
import ch.fhnw.ether.render.raytracer.renderers.ParallelRayTracingRenderer;
import ch.fhnw.ether.render.raytracer.shaders.PhongShader;
import ch.fhnw.ether.scene.DefaultScene;
import ch.fhnw.ether.scene.I3DObject;
import ch.fhnw.ether.scene.IScene;
import ch.fhnw.ether.scene.camera.Camera;
import ch.fhnw.ether.scene.camera.ICamera;
import ch.fhnw.ether.scene.light.PointLight;
import ch.fhnw.ether.view.IView;
import ch.fhnw.ether.view.gl.DefaultView;
import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.Vec3;

public class ReflectionExample1 extends AbstractRaytracingExample {

	public static void main(final String[] args) {
		new ReflectionExample1();
	}

	public ReflectionExample1() {
		final int superSampling = getSuperSamplingRate();
		final int softShadowSampling = getSoftShadowRate();
		final int width = getResolutionX();
		final int height = getResolutionY();

		// load objects
		final List<I3DObject> objects = new ArrayList<>();

		// setup scene
		objects.add(new PointLight(new Vec3(0, -500, 5000), RGB.BLACK, RGB.WHITE, 30, 30));
		objects.add(new PointLight(new Vec3(0, 0, 1000), RGB.BLACK, RGB.GREEN, 30, 30));
		objects.add(new PointLight(new Vec3(-5000, -3000, 1000), RGB.BLACK, RGB.RED, 30, 30));
		objects.add(new PointLight(new Vec3(5000, -3000, 1000), RGB.BLACK, RGB.BLUE, 30, 30));

		objects.add(new ParametricRayTracingObject(new Vec3(300, 0, 100), new SphereSurface(100), new PhongShader(), new NewReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(-300, 0, 100), new SphereSurface(100), new PhongShader(), new NewReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(0, 1600, 500), new SphereSurface(500), new PhongShader(), new NewReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(500, 1000, 300), new SphereSurface(300), new PhongShader(), new NewReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(-500, 200, 250), new SphereSurface(250), new PhongShader(), new NewReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(-700, 50, 50), new SphereSurface(50), new PhongShader(), new NewReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(50, 500, 50), new SphereSurface(50), new PhongShader(), new NewReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(0, 10000, 10000), new SphereSurface(10000), new PhongShader(), new NewReflectiveMaterial()));
		objects.add(new ParametricRayTracingObject(new Vec3(0, 0, 0), new PlaneSurface(), new PhongShader(), new MattMaterial(new SimpleColorTexture(new RGB(0.2f, 0.2f, 0.2f)), 1)));

		// create controller, camera, scene and view
		final AbstractRayTracingRenderer renderer = new ParallelRayTracingRenderer(superSampling, softShadowSampling);
		final IController controller = new DefaultController(new EventDrivenScheduler(), renderer);
		final IScene scene = new DefaultScene(controller);
		controller.setScene(scene);

		objects.stream().forEach(so -> scene.add3DObject(so));

		final Vec3 cameraPosition = new Vec3(0, -1000, 300);
		final Vec3 targetPosition = new Vec3(0, 0, 200);
		final Vec3 look = targetPosition.subtract(cameraPosition).normalize();
		final Vec3 rightVector = look.cross(Vec3.Y);
		final Vec3 up = look.cross(rightVector).scale(-1);
		final ICamera camera = new Camera(cameraPosition, Vec3.ZERO, up, 2.5f, 0.5f, Float.POSITIVE_INFINITY);
		final IView view = new DefaultView(controller, 50, 50, width, height, IView.MAPPED_VIEW, ExampleUtil.getWindowTitle(getClass().getName(), superSampling, softShadowSampling), camera);
		controller.addView(view);
	}

	private static class NewReflectiveMaterial extends ReflectiveMaterial {
		@Override
		public float getRefractiveIndex() {
			return 0f;
		}

		@Override
		public float getReflection() {
			return 1f;
		}

	}
}
