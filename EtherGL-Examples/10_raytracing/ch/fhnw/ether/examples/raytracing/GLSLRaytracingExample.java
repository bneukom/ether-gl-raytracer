/*
 * Copyright (c) 2013 - 2014 Stefan Muller Arisona, Simon Schubiger, Samuel von Stachelski
 * Copyright (c) 2013 - 2014 FHNW & ETH Zurich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *  Neither the name of FHNW / ETH Zurich nor the names of its contributors may
 *   be used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package ch.fhnw.ether.examples.raytracing;

import ch.fhnw.ether.controller.DefaultController;
import ch.fhnw.ether.controller.IController;
import ch.fhnw.ether.controller.event.EventDrivenScheduler;
import ch.fhnw.ether.render.raytracer.materials.IRayTracingMaterial;
import ch.fhnw.ether.render.raytracer.materials.builtin.MattMaterial;
import ch.fhnw.ether.render.raytracer.materials.textures.IRayTracingTexure;
import ch.fhnw.ether.render.raytracer.materials.textures.NormalMap;
import ch.fhnw.ether.render.raytracer.materials.textures.SimpleColorTexture;
import ch.fhnw.ether.render.raytracer.materials.textures.procedural.CheckBoardTexture;
import ch.fhnw.ether.render.raytracer.objects.RayTracingObject;
import ch.fhnw.ether.render.raytracer.objects.parametric.ParametricRayTracingObject;
import ch.fhnw.ether.render.raytracer.objects.parametric.surface.SphereSurface;
import ch.fhnw.ether.render.raytracer.renderers.GPURayTracingRenderer;
import ch.fhnw.ether.render.raytracer.shaders.PhongShader;
import ch.fhnw.ether.scene.DefaultScene;
import ch.fhnw.ether.scene.IScene;
import ch.fhnw.ether.scene.camera.Camera;
import ch.fhnw.ether.scene.camera.ICamera;
import ch.fhnw.ether.scene.light.ILight;
import ch.fhnw.ether.scene.light.PointLight;
import ch.fhnw.ether.view.IView;
import ch.fhnw.ether.view.gl.DefaultView;
import ch.fhnw.util.color.RGB;
import ch.fhnw.util.math.Vec3;

public final class GLSLRaytracingExample extends AbstractRaytracingExample {

	public static void main(String[] args) {
		new GLSLRaytracingExample();
	}

	public GLSLRaytracingExample() {
		final int width = 1820;
		final int height = 980;

		// Create controller
		IController controller = new DefaultController(new EventDrivenScheduler(), new GPURayTracingRenderer(width, height));

		// Create view
		final Vec3 cameraPosition = new Vec3(-1, 0, 1);
		final Vec3 targetPosition = new Vec3(0, 1, 1);
		final Vec3 look = targetPosition.subtract(cameraPosition).normalize();
		final Vec3 rightVector = look.cross(Vec3.Y);
		final Vec3 up = look.cross(rightVector).scale(-1);
		final ICamera camera = new Camera(cameraPosition, targetPosition, up, 2.5f, 0.5f, Float.POSITIVE_INFINITY);
		IView view = new DefaultView(controller, 50, 50, width, height, IView.INTERACTIVE_VIEW, getClass().getName(), camera);
		controller.addView(view);

		// Create scene and add triangle
		IScene scene = new DefaultScene(controller);
		controller.setScene(scene);

		for (int x = -4; x < 4; ++x) {
			for (int y = -2; y < 2; ++y) {
				final RayTracingObject sphere = new ParametricRayTracingObject(new Vec3(x * 4, y * 4, 15), new SphereSurface(1.5f), new PhongShader(), new MattMaterial(new SimpleColorTexture(new RGB((float) Math.random(), (float) Math.random(),
						(float) Math.random())), 0.4f));
				scene.add3DObject(sphere);
			}
		}

		final ILight light4 = new PointLight(new Vec3(1.5, 1, 5), RGB.BLACK, RGB.WHITE);
		scene.add3DObject(light4);
	}

}
